package fr.yobreuil.allomovies.ui.viewmodel

import androidx.lifecycle.ViewModel
import fr.yobreuil.allomovies.data.persistance.AlloMoviesDatabase
import fr.yobreuil.allomovies.data.repositories.GenreRepository

class GenreViewModel : ViewModel() {
    private val genreRepo = GenreRepository(AlloMoviesDatabase.getInstance().genreDao())

    val genreList = genreRepo.getAll()
}