package fr.yobreuil.allomovies

import android.app.Application
import fr.yobreuil.allomovies.data.persistance.AlloMoviesDatabase

class AlloMoviesApplication : Application() {
    override fun onCreate() {
        super.onCreate()
        AlloMoviesDatabase.initialize(this)
    }
}