package fr.yobreuil.allomovies.data.persistance.dao

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Query
import fr.yobreuil.allomovies.data.entities.GenreWithMovies
import fr.yobreuil.allomovies.data.entities.Movie

@Dao
interface MovieDao {
    @Query("SELECT * FROM movies")
    fun getAll() : LiveData<List<Movie>>

    @Query("SELECT * FROM movies ORDER BY movieName ASC")
    fun getAllAlphabetical() : LiveData<List<Movie>>

    @Query("SELECT * FROM movies ORDER BY movieName ASC")
    fun getMoviesByGenre(genreId: Long) : LiveData<GenreWithMovies>;
}