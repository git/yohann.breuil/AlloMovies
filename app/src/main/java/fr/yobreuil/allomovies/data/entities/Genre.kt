package fr.yobreuil.allomovies.data.entities

import androidx.room.Entity;
import androidx.room.PrimaryKey

const val GENRE_DEFAULT_ID = 0L

@Entity(tableName = "genres")
data class Genre(
    @PrimaryKey(autoGenerate = true)
    val genreId: Long = GENRE_DEFAULT_ID,
    var genreName: String = ""
)