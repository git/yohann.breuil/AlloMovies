package fr.yobreuil.allomovies.data.entities

import androidx.room.Embedded
import androidx.room.Junction
import androidx.room.Relation

data class GenreWithMovies(
    @Embedded val genre: Genre,
    @Relation(
        parentColumn = "genreId",
        entityColumn = "movieId",
        associateBy = Junction(GenreMovieCrossRef::class)
    )
    val movies : List<Movie>
)