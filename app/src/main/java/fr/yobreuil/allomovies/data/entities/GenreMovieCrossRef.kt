package fr.yobreuil.allomovies.data.entities

import androidx.room.Entity

@Entity(primaryKeys = ["genreId", "movieId"])
data class GenreMovieCrossRef(
    val genreId: Long,
    val movieId: Long
)
