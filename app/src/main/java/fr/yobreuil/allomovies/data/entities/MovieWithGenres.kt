package fr.yobreuil.allomovies.data.entities

import androidx.room.Embedded
import androidx.room.Junction
import androidx.room.Relation

data class MovieWithGenres(
    @Embedded val movie: Movie,
    @Relation(
        parentColumn = "movieId",
        entityColumn = "genreId",
        associateBy = Junction(GenreMovieCrossRef::class)
    )
    val genres : List<Genre>
)