package fr.yobreuil.allomovies.data.persistance

import android.app.Application
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import fr.yobreuil.allomovies.AlloMoviesApplication
import fr.yobreuil.allomovies.data.entities.Genre
import fr.yobreuil.allomovies.data.entities.Movie
import fr.yobreuil.allomovies.data.persistance.dao.GenreDao
import fr.yobreuil.allomovies.data.persistance.dao.MovieDao

private const val ALLO_MOVIES_DB_FILENAME = "allomovies.db"

@Database(entities = [Genre::class, Movie::class], version = 1, exportSchema = false)
abstract class AlloMoviesDatabase : RoomDatabase() {
    abstract fun genreDao(): GenreDao
    abstract fun movieDao(): MovieDao

    companion object {
        private lateinit var application: Application

        @Volatile
        private var INSTANCE: AlloMoviesDatabase? = null

        fun getInstance() : AlloMoviesDatabase {
            if (::application.isInitialized) {
                if (INSTANCE == null) {
                    synchronized(this) {
                        if (INSTANCE == null) {
                            INSTANCE = Room.databaseBuilder(
                                application.applicationContext,
                                AlloMoviesDatabase::class.java,
                                ALLO_MOVIES_DB_FILENAME
                            ).build()
                        }
                    }
                }
                return INSTANCE!!
            }
            else {
                throw RuntimeException("Database must be first initialized")
            }
        }

        @Synchronized
        fun initialize(app: AlloMoviesApplication) {
            if (::application.isInitialized)
                throw RuntimeException("Database must not be initialized twice")
            application = app
        }
    }
}