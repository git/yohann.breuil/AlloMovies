package fr.yobreuil.allomovies.data.repositories

import android.util.Log
import androidx.lifecycle.LiveData
import fr.yobreuil.allomovies.data.entities.Genre
import fr.yobreuil.allomovies.data.persistance.dao.GenreDao
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

class GenreRepository(private val genreDao: GenreDao) {
    suspend fun insert(genre: Genre) = withContext(Dispatchers.IO) { Log.i("GenreRepo", "INSERT"); genreDao.insert(genre) }
    suspend fun delete(genre: Genre) = withContext(Dispatchers.IO) { Log.i("GenreRepo", "DELETE"); genreDao.delete(genre) }
    suspend fun update(genre: Genre) = withContext(Dispatchers.IO) { Log.i("GenreRepo", "UPDATE"); genreDao.update(genre) }

    fun findById(genreId: Long): LiveData<Genre> {
        Log.i("GenreRepo", "FIND GENRE")
        return genreDao.getById(genreId)
    }

    fun getAll(): LiveData<List<Genre>> {
        Log.i("GenreRepo", "GET ALL GENRES")
        return genreDao.getAll()
    }
}