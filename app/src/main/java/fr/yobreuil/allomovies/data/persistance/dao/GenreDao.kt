package fr.yobreuil.allomovies.data.persistance.dao

import androidx.lifecycle.LiveData
import androidx.room.*
import fr.yobreuil.allomovies.data.entities.Genre
import fr.yobreuil.allomovies.data.entities.GenreWithMovies

@Dao
interface GenreDao {
    @Query("SELECT * FROM genres")
    fun getAll() : LiveData<List<Genre>>

    @Query("SELECT * FROM genres WHERE id = :id")
    fun getById(id : Long) : LiveData<Genre>

    @Query("SELECT")
    fun getGenreWithMovies(idGenre : Long)

    @Query("SELECT")
    fun getMoviesByGenre(genreId: Long): LiveData<GenreWithMovies>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insert(genre: Genre)

    @Insert
    suspend fun insertAll(vararg genres: Genre)

    @Update(onConflict = OnConflictStrategy.REPLACE)
    suspend fun update(genre: Genre)

    @Delete
    suspend fun delete(genre: Genre)
}