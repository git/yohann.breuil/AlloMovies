package fr.yobreuil.allomovies.data.entities

import androidx.room.Entity;
import androidx.room.PrimaryKey

const val MOVIE_DEFAULT_ID = 0L

@Entity(tableName = "movies")
data class Movie(
    @PrimaryKey(autoGenerate = true)
    val movieId: Long = MOVIE_DEFAULT_ID,
    val movieName: String = ""
)