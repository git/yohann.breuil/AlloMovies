package fr.yobreuil.allomovies.api

import fr.yobreuil.allomovies.data.entities.Genre
import retrofit2.Call
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class GenreManager {
    private val URL = "https://api.themoviedb.org/3/genre/movie/list?api_key=3fda2cd4981d6b7ee72244a2cdd869a3&language=fr-FR"

    fun getAllGenres() : Call<List<Genre>> {
        val retrofit = Retrofit.Builder()
            .baseUrl(URL)
            .addConverterFactory(GsonConverterFactory.create())
            .build()

        val service = retrofit.create(GenreService::class.java)

        return service.getGenres()
    }


}