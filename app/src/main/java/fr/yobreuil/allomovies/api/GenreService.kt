package fr.yobreuil.allomovies.api

import fr.yobreuil.allomovies.data.entities.Genre
import retrofit2.Call
import retrofit2.http.GET

interface GenreService {
    @GET("genre")
    fun getGenres() : Call<List<Genre>>
}